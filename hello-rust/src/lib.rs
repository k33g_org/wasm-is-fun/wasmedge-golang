use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn say(string_param: &str) -> String {
  println!("The Rust function say() received {}", string_param);
  let start = String::from("👋 hello");
  let end = String::from("😃");

  return format!("{} {} {}", start, string_param, end)
}

