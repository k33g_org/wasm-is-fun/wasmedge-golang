# Getting started

## Build

Use the `rustwasmc` command to build the Rust function into a WebAssembly bytecode file.

```
rustwasmc build
```
