package main

import (
	"fmt"
	"os"
	"github.com/second-state/WasmEdge-go/wasmedge"
)

func main() {
	/// Expected Args[0]: program name (./bindgen_funcs)
	/// Expected Args[1]: wasm or wasm-so file (rust_bindgen_funcs_lib_bg.wasm))

	/// Set not to print debug info
	wasmedge.SetLogErrorLevel()

	/// Create configure
	var conf = wasmedge.NewConfigure(wasmedge.WASI)

	/// Create VM with configure
	var vm = wasmedge.NewVMWithConfig(conf)

	/// Init WASI
	var wasi = vm.GetImportObject(wasmedge.WASI)
	wasi.InitWasi(
		os.Args[1:],     /// The args
		os.Environ(),    /// The envs
		[]string{".:."}, /// The mapping directories
		[]string{},      /// The preopens will be empty
	)

	/// Instantiate wasm
	vm.LoadWasmFile(os.Args[1])
	vm.Validate()
	vm.Instantiate()

	/// Run bindgen functions
	var res interface{}
	var err error

	/// say: array -> array
	res, err = vm.ExecuteBindgen("say", wasmedge.Bindgen_return_array, []byte("Bob Morane"))
	if err == nil {
		fmt.Println("Run bindgen -- say:", string(res.([]byte)))
	} else {
		fmt.Println("Run bindgen -- say FAILED")
	}

	vm.Delete()
	conf.Delete()
}
