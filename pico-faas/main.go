package main

import (
	//"fmt"
	"net/http"
	"os"
  "github.com/labstack/echo/v4"
  "github.com/labstack/echo/v4/middleware"
	"github.com/second-state/WasmEdge-go/wasmedge"
)

// https://gobyexample.com/http-servers



func main() {

	helloHandler := func(c echo.Context) error {

		wasmedge.SetLogErrorLevel()
		/// Create configure
		var conf = wasmedge.NewConfigure(wasmedge.WASI)
		/// Create VM with configure
		var vm = wasmedge.NewVMWithConfig(conf)
	
		/// Init WASI
		var wasi = vm.GetImportObject(wasmedge.WASI)
		wasi.InitWasi(
			os.Args[1:],     /// The args
			os.Environ(),    /// The envs
			[]string{".:."}, /// The mapping directories
			[]string{},      /// The preopens will be empty
		)
	
		/// Instantiate wasm
		vm.LoadWasmFile(os.Args[1])
		vm.Validate()
		vm.Instantiate()



		/// Run bindgen functions
		var res interface{}
		var err error

		/// say: array -> array
		res, err = vm.ExecuteBindgen("say", wasmedge.Bindgen_return_array, []byte("Bob Morane"))

		vm.Delete()
		conf.Delete()

		if err == nil {
			return c.String(http.StatusOK, string(res.([]byte))+"\n")

		} else {
			return c.String(http.StatusBadRequest, err.Error()+"\n")
		}

	}

  // Echo instance
  e := echo.New()

  // Middleware
  e.Use(middleware.Logger())
  e.Use(middleware.Recover())

  // Routes
  e.GET("/hello", helloHandler)

  // Start server
  e.Logger.Fatal(e.Start(":8080"))

}
// go run main.go hello.wasm 
